import Vue from 'vue'
import Router from 'vue-router'
// 导入刚刚写的组件
import home from '@/components/home'
import login from '@/components/login'

Vue.use(Router)

export default new Router({
  routes: [
    // 下面都是固定的写法
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/home',
      name: 'home',
      meta: {
        requireAuth: true,    //该路由项需要权限验证
      },
      component: home
    },
  ]
})
